FROM ubuntu:bionic

RUN apt-get update && apt-get install --no-install-recommends -y sudo wget g++ ca-certificates pkg-config bash git libtool make g++-mingw-w64-x86-64 && rm -rf /var/lib/apt/lists/*

